package src;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 * Created by Andrey_Vaganov on 12/5/2016.
 */
public class MainReader {

    /**
     * Формат даты
     */
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

    /**
     * Форматтер, используется для преобразования строк в даты и обратно
     */
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    // Инициализация логера
    private static final Logger log = Logger.getLogger(MainReader.class);


    /**
     * Точка входа в программу
     * @param args
     */
    public static void main(String[] args) {
        try {
            readFile();
        } catch (MyException e) {
            log.error("Это сообщение ошибки в методе main()", e);
            System.out.println("Собственный класс-исключение MyException обработан в методе main");
        }
    }

    /**
     * Метод для чтения дат из файла
     */
    private static void readFile() throws MyException {

        //Открываем потоки на чтение из файла
        BufferedReader byfReader;
        try (FileReader reader = new FileReader("src/main/java/src/fiple.txt")) {
            byfReader = new BufferedReader(reader);

            //Читаем первую строку из файла
            String strDate = byfReader.readLine();

            while(strDate != null) {

                //Преобразуем строку в дату
                try {
                    Date date = parseDate(strDate);
                    //Выводим дату в консоль в формате dd-mm-yy
                    System.out.printf("%1$td-%1$tm-%1$ty \n", date);
                } catch (ParseException e) {
                    System.out.println("Не удалось конвертировать дату");
                }

                //Читаем следующую строку из файла
                    strDate = byfReader.readLine();
            }
        } catch (IOException e) {
            log.error("Это сообщение ошибки метода readFile()", e);
            //Проброс исключения
            throw new MyException(e);
        }
    }

    /**
     * Метод преобразует строковое представление даты в класс Date
     * @param strDate строковое представление даты
     * @return
     */
    private static Date parseDate(String strDate) throws ParseException { 
        return dateFormatter.parse(strDate);
    }
}
